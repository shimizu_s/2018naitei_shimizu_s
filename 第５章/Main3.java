public class Main3{
 public static void main(String[] args){
  String title = "タイトル";
  String address = "aaaa@email.com";
  String text = "本文";
  email(address,text);
 }

 public static void email(String title,String address,String text){
  System.out.println(address+"にメールを送信しました");
  System.out.println("件名："+title);
  System.out.println("本文："+text);
 }

 public static void email(String address, String text){
  System.out.println(address+"にメールを送信しました");
  System.out.println("件名：無題");
  System.out.println("本文："+text);
 }
}