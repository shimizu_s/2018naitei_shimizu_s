public class Matango{
 //変数宣言
 int hp = 50;
 private char suffix;
 //Matangoメソッド
 public Matango(char suffix){
  this.suffix = suffix;
 }
 //attackメソッド
 public void attack(Hero h){
  System.out.println("キノコ"+this.suffix+"の攻撃");
  System.out.println("10のダメージ");
  h.setHp(h.getHp()-10);
 }
}