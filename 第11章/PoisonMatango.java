public class PoisonMatango extends Matango{
 //変数宣言
 private int count = 5;
 //PoisonMatangoメソッド
 public PoisonMatango(char suffix){
  super(suffix);
 }
 //追加行動
 public void attack(Hero h){
  super.attack(h);
  //回数制限判定
  if(this.count>0){
   System.out.println("さらに胞子をばらまいた！");
   int damage = h.getHp()/5;
   h.setHp(h.getHp()-damage);
   System.out.println(damage+"のダメージ");
   this.count--;
  }
 }
}