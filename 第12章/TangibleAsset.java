public abstract class TangibleAsset extends Asset implements Thing{
 //共通部分の変数

 /*12-2で不要となった
 private String name;
 private int price;
 */
 private String color;
 private double weight;
 //インスタンスごとに固有の値を代入
 public TangibleAsset(String name, int price, String color){
  super(name,price);
  this.color = color;
 }
 //getter

 /*12-2で不要となった
 public String getName(){
  return this.name;
 }
 public int getPrice(){
  return this.price;
 }
 */
 public String getColor(){
  return this.color;
 }
 public double getWeight(){
  return this.weight;
 }

 //setter
 public void setWeight(double weight){
  this.weight = weight;
 }
}