public class Main{
 public static void main(String[] args){
  //インスタンスの生成
  Book b = new Book("A",100,"Red","xxx-xxx-xxx");
  Computer c = new Computer("B",10000,"Blue","ABCD");

  //値の取得
  String bName = b.getName();
  int bPrice = b.getPrice();
  String bColor = b.getColor();
  String bIsbn = b.getIsbn();

  String cName = c.getName();
  int cPrice = c.getPrice();
  String cColor = c.getColor();
  String cMakerName = c.getMakerName();
  c.setWeight(3.14);

  //確認用表示
  System.out.println(bName+","+bPrice+","+bColor+","+bIsbn);
  System.out.println(cName+","+cPrice+","+cColor+","+cMakerName+","+c.getWeight());
 }
}