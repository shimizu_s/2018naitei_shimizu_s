public class Computer extends TangibleAsset{
 //コンピュータ固有の変数
 private String makerName;
 //コンピュータの情報の入力
 public Computer(String name,int price,String color,String makerName){
  super(name,price,color);
  this.makerName = makerName;
 }
 //コンピュータ固有の値のgetter
 public String getMakerName(){
  return this.makerName;
 }
}