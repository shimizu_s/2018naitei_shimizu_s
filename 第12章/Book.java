public class Book extends TangibleAsset{
 //本固有の変数
 private String isbn;
 //本の情報を入力
 public Book(String name,int price,String color,String isbn){
  super(name,price,color);
  this.isbn = isbn;
 }
 //本固有の値のgetter
 public String getIsbn(){
  return this.isbn;
 }
}