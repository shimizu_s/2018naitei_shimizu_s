public abstract class Asset{
 //変数宣言
 private String name;
 private int price;

 //コンストラクタ
 public Asset(String name,int price){
  this.name = name;
  this.price = price;
 }

 //getter
 public String getName(){
  return this.name;
 }
 public int getPrice(){
  return price;
 }
}