public class Cleric{
 //変数宣言
 String name;
 int hp = 50;
 int mp = 10;
 static final int MAX_HP = 50;
 static final int MAX_MP = 10;

 public Cleric( String name, int hp, int mp ){
  //インスタンスごとに値を代入
  this.name = name;
  this.hp = hp;
  this.mp = mp;
 }

 public Cleric( String name, int hp ){
  this( name, hp, MAX_MP );
 }

 public Cleric( String name ){
  this( name, MAX_HP, MAX_MP);
 }

 public void selfAid(){

  //MP5消費
  this.mp -= 5;

  //HPを最大にする
  this.hp = this.MAX_HP;

 }

 public int prey( int time ){

  //回復量の計算
  int heal_mp = new java.util.Random().nextInt(3) + time;

  if( this.MAX_MP-this.mp < heal_mp ){
   //回復量計算
   int heal = this.MAX_MP-this.mp;

   //回復処理
   this.mp = MAX_MP;
   return heal;
  }
  else{
   //回復処理
   this.mp += heal_mp;
   return heal_mp;
  }

 }
}