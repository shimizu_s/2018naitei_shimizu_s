public class Hero{

 String name;
 int hp;

 //コンストラクタ
 Hero( String name ){
  this.name = name;
  this.hp = 100;
 }

 //コンストラクタ
 Hero(){
  this("NO NAME");
 }

 public String getName(){
  return this.name;
 }
 public void setName( String name ){
  this.name = name;
 }

 public int getHp(){
  return this.hp;
 }
 public void setHp( int hp ){
  //負数なら0を代入する
  if( hp < 0 ){ this.hp = 0; }
  else{ this.hp = hp; }
 }

}