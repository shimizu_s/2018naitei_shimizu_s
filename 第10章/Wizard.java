public class Wizard{

 //変数宣言
 private int hp;
 private int mp;
 private String name;
 private Wand wand;

 //回復関数
 public void heal( Hero h ){
  int basePoint = 10;
  int recovPoint = (int)(basePoint * this.getWand().getPower());

  h.setHp(h.getHp() + recovPoint);
  System.out.println(h.getName() + "のHPを" + recovPoint +"回復した！");
 }

 //getter
 public int getHp(){
  return this.hp;
 }
 public int getMp(){
  return this.mp;
 }
 public String getName(){
  return this.name;
 }
 public Wand getWand(){
  return wand;
 }

 //setter
 public void setHp( int hp ){
  //負数なら0を代入する
  if( hp < 0 ){ this.hp = 0; }
  else{ this.hp = hp; }
 }
 public void setMp( int mp ){
  //負数ならエラー
  if( mp < 0 ){
   throw new IllegalArgumentException("MPは０以上にしてください。");
  }
  this.mp = mp;
 }
 public void setName( String name ){
  //未入力または3文字未満でエラー
  if( name == null || name.length() < 3 ){
   throw new IllegalArgumentException("魔法使いの名前が入力されていないか、短すぎます。");
  }
  this.name = name;
 }
 public void setWand( Wand wand ){
  //未入力でエラー
  if( wand == null ){
   throw new IllegalArgumentException("杖がNULLです。");
  }
  this.wand = wand;
 }

}