public class Wand{

 //変数宣言
 private String name;
 private double power;

 //getter
 public String getName(){
  return this.name;
 }
 public double getPower(){
  return this.power;
 }

 //setter
 public void setName( String name ){
  //未入力または3文字未満でエラー
  if( name == null || name.length() < 3 ){
   throw new IllegalArgumentException("杖の名前が入力されていないか、短すぎます。");
  }
  this.name = name;
 }
 public void setPower( double power ){
  //0.5未満または101以上でエラー
  if( power < 0.5 || power > 100 ){
   throw new IllegalArgumentException("魔力は0.5〜100にしてください。");
  }
  this.power = power;
 }

}